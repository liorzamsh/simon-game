

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char *session;


char generate_random_color() {
    char colors[] = {'r', 'g', 'b', 'y'};
    int randomIndex = rand() % 4; // Generate a random index between 0 and 3
    return colors[randomIndex]; // Return the character at the random index
}

char* random_new_session()
{
    char randomletter;
	session = (char*)malloc(100);
	
    for (size_t i = 0; i < 100; i++)
    {
        session[i] = generate_random_color();
    }

	return session;
}


void show_current_stage(int stage)
{
     printf("stage: %d\n", stage);
    for (size_t i = 0; i <= stage; i++)
    {
        printf("%c  ", session[i]);
        fflush(stdout);
        sleep(2);
    }
    printf("\n");
    sleep(3);
    system("clear");
}

void get_press(char *press)
{
    printf("press the next move:\n");
    scanf("%s", press);
    printf("-----\n");
    return;
}


void stage(int stg)
{
    printf("stage: %d\n", stg);
	char c[100] = {0}; //100 to protect from overflow


	for(int i = 0; i <= stg; i++)
	{
        //get input
        get_press(&c);

        if(c[0] == session[i])
        {
            printf("correct!\n");
			continue; //correct!
        }
		//else exit
		else
		{
            printf("wrong!\n");
            free(session);
			exit(1); //failure
		}
	}	
}



int main(int argc, char const *argv[])
{
    //randomize new session 
	char* current_session = random_new_session();
	
	for(int i = 0; i < 100; i++)
	{
		//show the user the current stage -- ?
		show_current_stage(i);

		stage(i);
	}

	
	//success

    return 0;
}
